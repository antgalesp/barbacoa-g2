== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Cucharas
* Cucharillas
* Cucharillas largas para remover
* Cuchillos
* Papel aluminio para asar
* Platos
* Servilletas
* Tenedores
* Vasos

=== Mobiliario

* Hamaca
* Mesas
* Mesa supletoria para juegos
* Parrilla
* Sillas
* Sombrillas

=== Diversión

* Altavoces
* Balón de fútbol
* Balón de rugby
* Dados de rol
* Radiocassette
* Slack line
* Soga
